# Using with Python console and external interpreter in QiTissue (Deprecated)
## Installation
### Via external Python interpreter
- Install Python 3.9
- install prepacked-stardist
    ```bash
    pip install prepacked-stardist --index-url https://__token__:<your_personal_token>@gitlab.claiv.de/api/v4/projects/209/packages/pypi/simple
    ```
- change python binary in MacsIQView to installed python
![](images/python-settings.png)
- restart MaxcIQView

## Usage
```python
from prepacked_stardist import Stardist

algorithm = Stardist()

result = algorithm.predict( np_array )
```

## Predicting on MacsIQView data
```python
import numpy as np
from prepacked_stardist import Stardist
from PyQiTissue import QiApi

# Get the current dataset in QiTissue
dataset = QiApi.Dataset.currentDataset()

# Get the first channel in the dataset (assumed to be DAPI)
dapi = QiApi.Channel.channels(dataset)[0]

# Get the buffer for the DAPI channel
bufferHandle = QiApi.Channel.buffer(dapi)
buffer = memoryview(bufferHandle)

# Load the buffer data into a numpy array
nparray = np.frombuffer(buffer, dtype=np.uint16)
nparray = nparray.reshape(buffer.shape)

# Create a Stardist model
model = Stardist()

# Make predictions on your image data
result = model.predict(nparray)
```

In this example, we first use the QiApi to get the current dataset and the DAPI channel in the dataset. We then use the buffer() method provided by the QiApi.Channel class to get the data for the DAPI channel as a memory buffer, and load that buffer data into a numpy array.

Once we have our image data in a numpy array, we create a Stardist model using the Stardist class provided by the prepacked stardist package, and use the predict() method of the Stardist class to make predictions on our image data.

## Visualizing Results with Matplotlib
```python
import matplotlib.pyplot as plt
from stardist.plot import render_label

# Display the input image and the prediction overlaid on top of it
plt.subplot(1,2,1)
plt.imshow(nparray, cmap="gray")
plt.axis("off")
plt.title("Input Image")

plt.subplot(1,2,2)
plt.imshow(render_label(result, img=nparray))
plt.axis("off")
plt.title("Prediction + Input Overlay")

plt.show()
```