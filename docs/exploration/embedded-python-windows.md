# Installing in QiTissue Python on Windows

## Using the embedded python (same as on other platforms)
Use embedded Python by going to QiTissue -> Edit -> Preferences -> Python -> Details -> Run Terminal
![](images/run-terminal.png)

## Preparing environment
### Executable
Same as on Mac the QiTissues python can't be called using `python`, because it is apparently not in the path after clikcing the *Run terminal* button.

It can however be used by navigating to `C:/Program Files/QiTissue/Python3/` and using the `python.exe` / adding this to the path.

### Pip
Same as on Linux the bundled Python is missing some modules
![](images/windows-modules.png)

As suggested [here](https://stackoverflow.com/questions/42666121/pip-with-embedded-python) it is possible to use pip by uncommenting `import site` in pythonXX._pth and then runnign get-pip.py.

## Installing
The package can then be installed normally
```bash
python.exe -m pip install prepacked-stardist --index-url https://__token__:$GITLAB_TOKEN@gitlab.claiv.de/api/v4/projects/209/packages/pypi/simple
```
**Important: use python -m pip to use the correct pip**

**At this point Stardist was usable on Windows**