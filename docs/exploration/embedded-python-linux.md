# Approach of installing with python embedded in QiTissue
Idea: Use QiTissue embedded python as suggested, potentially create a virual environment with that version and install the algorithm in there

## Using the embedded python
Use embedded Python by going to QiTissue -> Edit -> Preferences -> Python -> Details -> Run Terminal
![](images/run-terminal.png)

## Preparing environment
- the embedded python is missing dependencies. i.e. the pip module will fail with `ModuleNotFoundError: No module named 'distutils.cmd` which is a default module, things like ensurepip are also not available.
- As a workaround the modules from local python3.9 installation can be used, by specifying th path in the custom modules
![](images/custom-modules.png)

## Venv
- Observation: Pythons sys._base_executable (used for venvs) is set to the Appimage QiTissue when starting python in the Terminal
    - this should be set to the python executable, venv creation won't work like this
    - Workaround 1: Set manually in python then create venv
    ```python
    import sys
    from venv import create
    import os

    # deduce executable path from os.__file__
    sys._base_executable='pathlib.Path(os.__file__).parents[2] / 'bin' / 'python'
    create('/path/to/venv')
    ```

    - Workaround 2: start a new shell then the path seems to be correct
    ```bash
    bash
    python -m venv /path/to/venv --without-pip
    ```
- Installing with pip did not work, somehow it could not find the ensurepip module. Instead pip can be installed after creating the venv:
```bash
source /path/to/venv/bin/activate
wget https://bootstrap.pypa.io/get-pip.py
python ./get-pip.py
```
## install prepacked-stardist
```bash
python -m pip install prepacked-stardist --index-url https://__token__:$GITLAB_TOKEN@gitlab.claiv.de/api/v4/projects/209/packages/pypi/simple
```
**Important: use python -m pip to use the correct pip**

### Problems
- Installing leads to some deeper problems, the install fails at jax, a dependency of tensorflow
```bash
File "/tmp/pip-build-env-9yeqmkst/overlay/lib/python3.9/site-packages/wheel/macosx_libfile.py", line 43, in <module>
    import ctypes
File "/tmp/.mount_MACSiQJ1He9b/usr/lib/python3.9/ctypes/__init__.py", line 8, in <module>
    from _ctypes import Union, Structure, Array
ImportError: libffi.so.6: cannot open shared object file: No such file or directory
[end of output]
  
note: This error originates from a subprocess, and is likely not a problem with pip.
error: metadata-generation-failed

```
- This seems to be a problem with the build version of python, the library version of libffi is not on the current system and QiTissue doesn't bundle it.

(scipy, tensorflow also breaks when using cached version of the packages already on the system)

- might be solved by bundling libffi6 with QiTissue (for all os'?)
- solved locally by installing the appropriate version of this library


## Conclusions

**At this point it was possible to import Stardist and make a prediction in the python environment**

- likely will run into **low level** platform and dependency specific issues that need to be taken into account when bundling python with QiTissue. (Like the libffi issue [problems](#problems))
- pip, venv and its dependencies need to be bundled with QiTissue to work properly in the embedded python ([venv](#venv))

## Approaches
- Test for different platforms, bundle everything needed with QiTissue
    - install and use algorithms directly in python environment
    - install and use venv to sandbox to avoid dependency conflicts -> call subprocess
    - bundle algorithms with QiTissue
- bundle algorithms into external binary that QiTissue downloads and calls (e.g. with pyinstaller)
    - same approach as venv but using own bundled python
    - need to test, build, bundle dependencies for different platforms
    - Probably also license problems