# Exploration of using prepackaged algorithms with QiTissue

We want to give users of QiTissue the ability to use Segmentation Algorithms like Stardist and Cellpose.

## Goals
- must be delivered independently of QiTissue, can't be bundled because of Licenses
    - Offer a simple way of installing the algorithms and their dependencies that is initiated **by the user**
- make the algorithms callable from QiTissue

## Approach
To accomplish these goals we chose to provide pip packages for the algorithms that provide a unified interface. 

This approach also provides a standardized way of declaring and installing the individual dependencies of the algorithms. Using pip the dependencies will automatically be installed with the provided package.

These packages also need to be public for the users to access them, so the projects and their artifacts will be hosted on a public gitlab instance as Open Source projects.

## Challenges
- The algorithms must work with the embedded QiTissue python environment
    - modules like pip and venv are currently missing (this is apparently standard for embedded python [this](https://stackoverflow.com/questions/42666121/pip-with-embedded-python) may be interesting)
- The algorithms might have low-level dependencies that need to be accounted and tested for (on the target platforms)
    - e.g. problems with tensorflow: system libraries and architecture (arm mac)
- Especially with more algorithms there will be dependency conflicts between them
    - the algorithms should be installed in their own virtual environments 
        - this is standard python
        - need to be called as a subrpocess
        - prepackaged-stardist contains a minimal cli, as a proof of concept, that can do a prediction on a .npy file

## Exploration

### Packages
So far we have prepared 2 projects: prepacked-stardist, prepacked-cellpose. These provide two pip packages with the required dependencies as well as a simple interface for instantiating the algorithms and making a prediction. 

These algorithms currently offer an interface that expects a numpy array and also returns a numpy array.
```python
# predict function signature
def predict( self, input: np.ndarray[(np.uint16, np.uint16)] ) -> np.ndarray[(int, int)]
```

**Example using Stardist after installation:**
```python
from prepacked_stardist import Stardist

algorithm = Stardist()

result = algorithm.predict( np_array )
```

This can be visualized using matplotlib
```python
import matplotlib.pyplot as plt
from stardist.plot import render_label

plt.subplot(1,2,1)
plt.imshow(np_array, cmap="gray")
plt.axis("off")
plt.title("Input Image")

plt.subplot(1,2,2)
plt.imshow(render_label(result, img=np_array))
plt.axis("off")
plt.title("Prediction + Input Overlay")

plt.show()
```

for testing purposes Stardist provides an input image
```python
from stardist.data import test_image_nuclei_2d
np_array = test_image_nuclei_2d()
```

### QiTissue
As a proof of concept we explored the installation and usage of these prepared packages in conjunction with QiTissue its python environment.
- [Installation on linux using the embedded QiTissue Python environment](./embedded-python-linux.md)
- [Installation on Windows using the embedded QiTissue Python environment](./embedded-python-windows.md)
- [Installation and usage on linux using an external Python environment (Deprecated)](./external-python-apporach.md)
    - We don't want to follow this approach because the user should not need to use a separate Python environment, also this might lead to other conflicts
