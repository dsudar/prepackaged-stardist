# [](https://gitlab.com/claiv/prepackaged-stardist/compare/v2.1.0...v) (2023-10-30)



# [2.1.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v2.0.0...v2.1.0) (2023-10-30)


### Features

* **cli:** add exlicit option to not use gpu, use it in tests for correct result ([fecd095](https://gitlab.com/claiv/prepackaged-stardist/commit/fecd09527c5c91cab04ff44f77550218ed83b45c))
* **logging:** added logging for stardist prediction ([c295e38](https://gitlab.com/claiv/prepackaged-stardist/commit/c295e38082ebbb9fbeade87d909d2103dc26ec83))



# [2.0.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v1.2.2...v2.0.0) (2023-07-14)


* feat!: add option to supply a model folder containing a trained stardist model ([65465bd](https://gitlab.com/claiv/prepackaged-stardist/commit/65465bd7d712d3aaebb1d09394e52e510ecf57a0))
* refactor(cli)!: rename to stardist-cli for convention’s sake ([63c8d0c](https://gitlab.com/claiv/prepackaged-stardist/commit/63c8d0c130d5af9ec670da4e1e86fab97a780990))


### BREAKING CHANGES

* the cli and python api for supplying the model changed. It now takes either model_name for pretrained models or model_folder for supplying a model from disk
* cli now called stardist-cli instead of stardist



## [1.2.2](https://gitlab.com/claiv/prepackaged-stardist/compare/v1.2.1...v1.2.2) (2023-07-14)


### Bug Fixes

* **dependency:** pin tensorflow version because of 2.13 incompatibility ([ec97e70](https://gitlab.com/claiv/prepackaged-stardist/commit/ec97e700b0c1ec9bbb2c26cc2430a42b52e99ef9))



## [1.2.1](https://gitlab.com/claiv/prepackaged-stardist/compare/v1.2.0...v1.2.1) (2023-05-29)


### Bug Fixes

* **version:** change python version for more compatibility ([2291043](https://gitlab.com/claiv/prepackaged-stardist/commit/22910433795569c81e0bf12bbce56a13a4505256))



# [1.2.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v1.1.0...v1.2.0) (2023-05-25)


### Features

* **cli:** add functionality to use tiff file as output by using an output file with tiff ending. ([587b18c](https://gitlab.com/claiv/prepackaged-stardist/commit/587b18c5827b3e12a365faaa93e95ffd504e34a5))



# [1.1.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v1.0.1...v1.1.0) (2023-05-25)


### Features

* **cli:** add fully featured cli, that can be called via stardist console_script. Add test for cli ([c24a9d7](https://gitlab.com/claiv/prepackaged-stardist/commit/c24a9d77a518146c5925358f7b183d7df839293a))



## [1.0.1](https://gitlab.com/claiv/prepackaged-stardist/compare/v1.0.0...v1.0.1) (2023-05-11)


### Bug Fixes

* bump version for new dependency ([4845317](https://gitlab.com/claiv/prepackaged-stardist/commit/4845317a9652cdaebf7ca9fe5ddd38c3373eb7ab))



# [1.0.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.5.2...v1.0.0) (2023-05-08)


### Code Refactoring

* **algorithm:** remove bundled verstile fluo model, remove normalize option from predict always normalizing ([2873462](https://gitlab.com/claiv/prepackaged-stardist/commit/2873462f4fb66eeeaa2b92e5281894c6e4bd9fe8))


### BREAKING CHANGES

* **algorithm:** changes constructor and predict parameters



## [0.5.2](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.5.1...v0.5.2) (2023-05-08)


### Bug Fixes

* change to sourcepath layout, including tests in package that way ([809599e](https://gitlab.com/claiv/prepackaged-stardist/commit/809599e479e941762b71d2bd74497290809c98fc))



## [0.5.1](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.5.0...v0.5.1) (2023-05-08)


### Bug Fixes

* add init py to tests ([dea2b80](https://gitlab.com/claiv/prepackaged-stardist/commit/dea2b80c86964313603af0967f8207d5be8d6e07))



# [0.5.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.4.0...v0.5.0) (2023-05-08)


### Features

* add tests to distribution to allow testing the package ([32ebded](https://gitlab.com/claiv/prepackaged-stardist/commit/32ebded0adafcccae7753bbd1a90e34ebad58f1d))



# [0.4.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.3.1...v0.4.0) (2023-04-22)


### Features

* **cli:** add cli for stardist that can be bundled into a binary using pyinstaller ([7ff1755](https://gitlab.com/claiv/prepackaged-stardist/commit/7ff1755d1e189eabd4f7230e1b462de7fd651958))



## [0.3.1](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.3.0...v0.3.1) (2023-04-20)


### Bug Fixes

* **build:** remove readme from pyproject.toml, as the contents have been moved to docs ([9af9f01](https://gitlab.com/claiv/prepackaged-stardist/commit/9af9f013a3741cc577950db46fc8407e49f0a255))



# [0.3.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.2.4...v0.3.0) (2023-04-20)


### Features

* **stardist:** the model of stardist can now be chosen via the constructor, including the new bundled version ([3fc6c84](https://gitlab.com/claiv/prepackaged-stardist/commit/3fc6c845a5091f03bc6ec9713c8dcc60787cc7ed))



## [0.2.4](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.2.3...v0.2.4) (2023-04-05)


### Bug Fixes

* added TF to the dependencies to auto-install it for non M1 and M2 users. ([0116a9a](https://gitlab.com/claiv/prepackaged-stardist/commit/0116a9ad80a5f5967762095879c75a68007c7ec0))



## [0.2.3](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.2.2...v0.2.3) (2023-04-04)


### Bug Fixes

* fixed normalize import ([628e143](https://gitlab.com/claiv/prepackaged-stardist/commit/628e1439744287d9932e69bcba465de0dd209402))



## [0.2.2](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.2.1...v0.2.2) (2023-04-04)


### Bug Fixes

* qi/qi_cloud/prepackaged-algorithms[#1](https://gitlab.com/claiv/prepackaged-stardist/issues/1) ([ece32f3](https://gitlab.com/claiv/prepackaged-stardist/commit/ece32f348d1911ac50ea4881d39547631eef91d5))



## [0.2.1](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.2.0...v0.2.1) (2023-04-04)


### Bug Fixes

* qi/qi_cloud/prepackaged-algorithms[#1](https://gitlab.com/claiv/prepackaged-stardist/issues/1) ([62b0295](https://gitlab.com/claiv/prepackaged-stardist/commit/62b0295b3c3f18beff917098e4a9e635eb687a54))



# [0.2.0](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.1.2...v0.2.0) (2023-03-31)


### Features

* **predict:** add option to normalize input data, fix test case ([ae5cd29](https://gitlab.com/claiv/prepackaged-stardist/commit/ae5cd29f2dc1ca96f71816985bb0601cb8f7511d))



## [0.1.2](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.1.1...v0.1.2) (2023-03-31)


### Bug Fixes

* **ci:** also publishing to gitlab registry ([437ac3e](https://gitlab.com/claiv/prepackaged-stardist/commit/437ac3ef9e282e9a8b4c3b8968acb3f02ac2214c))



## [0.1.1](https://gitlab.com/claiv/prepackaged-stardist/compare/v0.1.0...v0.1.1) (2023-03-31)


### Bug Fixes

* **ci:** add some semantic release config, changed commit message to skip ([a61abd5](https://gitlab.com/claiv/prepackaged-stardist/commit/a61abd569ffc418a2ffcdf01e970183548dbb3d1))



# [0.1.0](https://gitlab.com/claiv/prepackaged-stardist/compare/095138831162c05e3add66de17a4fdb8be955204...v0.1.0) (2023-03-31)


### Bug Fixes

* **ci:** add commit author, commiter identity ([0e6b878](https://gitlab.com/claiv/prepackaged-stardist/commit/0e6b878ac0317f358088b1044a2ee30f253ac152))
* **ci:** change commit_author to string ([a2b5287](https://gitlab.com/claiv/prepackaged-stardist/commit/a2b5287377b128b5057cb4f53024c649574e4765))
* **ci:** fix initial version for semantic release ([a383a99](https://gitlab.com/claiv/prepackaged-stardist/commit/a383a995c77a8de97603b1552b25d495bf4a3a4f))
* **ci:** fix typo ([500a9e8](https://gitlab.com/claiv/prepackaged-stardist/commit/500a9e87b78c4f9fe08b12986ae56311fc2ac6ac))


### Features

* add semantic release config ([4b219f4](https://gitlab.com/claiv/prepackaged-stardist/commit/4b219f41dfd5feef365d96851db972151c1e7f3a))
* **stardist:** add simple stardist implementation, add simple test case for it ([0951388](https://gitlab.com/claiv/prepackaged-stardist/commit/095138831162c05e3add66de17a4fdb8be955204))



