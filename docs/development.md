# Development
**This is the Readme for developing this package, for information on usage refer to [docs](README.md)**

## Install
### Install Poetry
Install poetry:
```bash
curl -sSL https://install.python-poetry.org | python3 -
```
or refer to the [poetry docs](https://python-poetry.org/docs/)

### Install Dependencies
Use `poetry install` to install the dependencies. 

Poetry will create a project local virtual environment in `.venv`, this needs to be activated to use the installed 
packages. You can do this manually with `source .venv/bin/activate` (Unix like) or `.venv/Scripts/activate` (Windows) or via the IDE of your choice, e.g. for 
Visual Studio Code refer to [select and activate a virtual environment](https://code.visualstudio.com/docs/python/environments#_select-and-activate-an-environment).

## Publishing
Because we currently have no project or group level access tokens on gitlab.com, we can't use semantic-release in the pipeline.

The pipeline uses poetry to build and release a pip package in the package registry. **The version numbering will not be automatically generated**. 

Instead run semantic-release locally to create the appropriate version (in the venv):
```bash
python -m semantic_release version
```
this will update the version number, create a tag, and commit

Optional: To also create a changelog it is possible to use conventional changelog
```bash
npm i conventional-changelog-cli
conventional-changelog -p angular -i docs/CHANGELOG.md -s -r 0
```

Finally push the commits + tags (tags are not pushed automatically and are useful for releases and changelog generation)
```bash
git push --follow-tags
```