# Prepackaged Stardist for QiTissue

This is a package intended for installing Stardist with QiTissue. The main purpose is to be able to install all dependencies easily and to provide an interface for QiTissue to use.

## Installation
This package can be installed using pip from the gitlab pypi registry
```bash
pip install prepacked-stardist --index-url https://gitlab.com/api/v4/groups/claiv/-/packages/pypi/simple
```
### Known Issues
- Does not work on M1/M2 Macs because a different version of tensorflow is necessary

## Testing
The package includes some tests that can be run to make sure the package is working properly on the  platform and environment.
```bash
pip install pytest
pytest --pyargs prepacked_stardist
```
This will run some predictions on known data and compares them with the expected outcome

## CLI
This package contains a cli as a Command Line Script. After installing `stardist-cli` should be available in the console. And can be used with the following arguments:

```bash
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        the path to the input numpy array (.npy)
  -o OUTPUT, --output OUTPUT
                        the path to the output file. Supports numpy array (.npy) and tiff file (.tiff/.tif)
  -m {2D_versatile_fluo,2D_paper_dsb2018}, --model_name {2D_versatile_fluo,2D_paper_dsb2018}
                        the model to use
  -f MODEL_FOLDER, --model_folder MODEL_FOLDER
                        a folder containing a pretrained stardist model
```


Example usage:
```bash
stardist-cli -i path/to/input/file.npy -o file path/to/output/file.npy -m 2D_versatile_fluo
```
The model can also bes specified via providing the path to a stardist model:
```bash
stardist-cli -i path/to/input/file.npy -o file path/to/output/file.npy -f /path/to/model/folder
```

## Usage and Parameters
### Instantiation
The package provides a Stardist class that can be imported and instantiated:
```python
from prepacked_stardist import Stardist
algorithm = Stardist(model_name='2D_versatile_fluo')
```
the contructor provides one parameter that lets the user choose the pretrained model to use out of 
`['2D_versatile_fluo', '2D_paper_dsb2018']` (see [here](https://github.com/stardist/stardist#pretrained-models-for-2d) for more info)
stardist will download the data for the model if they are not already downloaded.

Alternatively a path to the folder of a model can be used, to do this provide the model folder
```python
algorithm = Stardist(model_folder='/path/to/model')
```

### Prediction
A prediction can be made by calling predict on the instance

```python
result = algorithm.predict( np_array )
```

the function takes a numpy array and returns a numpy array. Specifically the signature is 

```python
def predict( self, input: np.ndarray[(np.uint16, np.uint16)] ) -> np.ndarray[(int, int)]
```

## CLI



