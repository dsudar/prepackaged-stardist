from typing import Literal
import logging
import numpy as np
import os


class Stardist:
    def __init__(self, model_name: Literal['2D_versatile_fluo', '2D_paper_dsb2018'] = None, model_folder=None, use_gpu: Literal['auto', 'no'] = 'auto'):
        """
        :param model_name: the pretrained model to use default '2D_versatile_fluo' (https://github.com/stardist/stardist#pretrained-models-for-2d)
        :param model_folder: 
        """

        assert \
            model_name in ['2D_versatile_fluo', '2D_paper_dsb2018'] and model_folder is None or \
            model_name is None and model_folder is not None \
            , 'Supply either model_name or model_folder'

        if use_gpu == 'no':
            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        from stardist.models import StarDist2D

        if model_name:
            logging.info(f'initializing with given given pretrained model {model_name}')
            self.model = StarDist2D.from_pretrained(model_name)
        elif model_folder:
            logging.info(f'initializing with given model folder {model_folder}')
            basedir, name = os.path.split(model_folder)
            self.model = StarDist2D(None, basedir=basedir, name=name)



    def predict(self, input: np.ndarray[(np.uint16, np.uint16)]) -> np.ndarray[(int, int)]:
        """
        :param input: input image (2d greyscale image)
        :return: the prediction mask as numpy array
        """
        from csbdeep.utils import normalize as normalize_data
        logging.debug("normalizing input data")
        input_data = normalize_data(input)
        logging.debug("normalized input data")
        logging.debug("starting Stardist prediction")
        labels, _ = self.model.predict_instances(input_data)
        logging.info("finished Stardist prediction")
        return labels
