import argparse
import logging
import numpy as np
from prepacked_stardist import Stardist
import tifffile as tiff
import sys

def cli():
    logging.getLogger().setLevel(logging.INFO)
    print(sys.argv)
    parser = argparse.ArgumentParser(description='stardist cli')
    parser.add_argument('-i', '--input', help='the path to the input numpy array (.npy)', required=True)
    parser.add_argument('-o', '--output', help='the path to the output file. Supports numpy array (.npy) and tiff file (.tiff/.tif)', required=True)
    parser.add_argument('-m', '--model_name', help='the model to use',
                        choices=['2D_versatile_fluo', '2D_paper_dsb2018'], default=None)
    parser.add_argument('-f', '--model_folder', help='a folder containing a pretrained stardist model', default=None)
    parser.add_argument('-g', '--gpu', help='if gpu should be used, defaults to auto', choices=['auto', 'no'],
                             default='auto')

    # Parse the command-line arguments and options and call the appropriate function
    args = parser.parse_args()

    model_name = args.model_name
    model_folder = args.model_folder
    if model_folder is None and model_name is None:
        model_name = '2D_versatile_fluo'

    input_file = np.load(args.input)
    logging.debug("initializing Stardist model")
    algorithm = Stardist(model_name=model_name, model_folder=model_folder, use_gpu=args.gpu)
    logging.info("initialized Stardist model")
    labels = algorithm.predict(input_file)

    output_path = args.output
    file_extension = output_path.split('.')[-1].lower()
    logging.debug(f'try saving predictions at {output_path}')
    if file_extension == 'tif' or file_extension == 'tiff':
        tiff.imwrite(output_path, labels.astype(np.uint16))  # Save labels as TIFF file
        logging.info(f'saved predictions as tif file at {output_path}')
    else:
        np.save(output_path, labels)  # Save labels as NPY array
        logging.info(f'saved predictions as numpy array at {output_path}')

