from pathlib import Path
from typing import ContextManager

import pytest
import importlib.resources
from .. import data
import numpy as np

@pytest.fixture
def ex_input_data_path() -> 'ContextManager[Path]':
    return importlib.resources.path(data, 'test_input.npy')

@pytest.fixture
def ex_prediction_data() -> np.ndarray[(int, int)]:
    with importlib.resources.open_binary(data, "demo_stardist_prediction.npy") as f:
        example_prediction = np.load(f)
    assert example_prediction.ndim == 2
    return example_prediction

@pytest.fixture
def ex_prediction_data_gpu() -> np.ndarray[(int, int)]:
    with importlib.resources.open_binary(data, "demo_stardist_prediction_gpu.npy") as f:
        example_prediction = np.load(f)
    assert example_prediction.ndim == 2
    return example_prediction