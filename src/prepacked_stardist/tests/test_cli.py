import logging
import pathlib
import shutil

import pytest
from .fixtures.data import ex_prediction_data, ex_input_data_path, ex_prediction_data_gpu
import subprocess
import numpy as np
import os
import tifffile as tiff
import zipfile
import requests


@pytest.mark.usefixtures('ex_prediction_data', 'ex_input_data_path')
def test_prediction_default(ex_prediction_data, ex_input_data_path):
    with ex_input_data_path as path:
        ex_output_data_path = './ex_output_data.npy'
        subprocess.run(f'stardist-cli -i {path.absolute()} -o {ex_output_data_path} --gpu no', shell=True)
    
    labels = np.load(ex_output_data_path)

    assert np.array_equal(labels, ex_prediction_data)

    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_prediction_data', 'ex_input_data_path')
def test_prediction_default_tiff(ex_prediction_data, ex_input_data_path):
    with ex_input_data_path as path:
        ex_output_data_path = './ex_output_data.tiff'
        subprocess.run(f'stardist-cli -i {path.absolute()} -o {ex_output_data_path} --gpu no', shell=True)

    labels = np.array(tiff.imread(ex_output_data_path))

    assert np.array_equal(labels, ex_prediction_data)

    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_prediction_data', 'ex_input_data_path')
def test_model_folder(ex_prediction_data, ex_input_data_path):

    url = 'https://github.com/stardist/stardist-models/releases/download/v0.1/python_2D_versatile_fluo.zip'
    logging.info(f'downloading model {url}')
    model_zip = "./model.zip"
    model_folder = "./model"
    response = requests.get(url)
    with open(model_zip, 'wb') as file:
        file.write(response.content)

    # Extract the zip file
    with zipfile.ZipFile(model_zip, 'r') as zip_ref:
        zip_ref.extractall(model_folder)
    os.remove(model_zip)

    model_folder_path = pathlib.Path(model_folder)

    with ex_input_data_path as path:
        ex_output_data_path = './ex_output_data.npy'
        result = subprocess.run(f'stardist-cli -i {path.absolute()} -o {ex_output_data_path} -f {model_folder_path.absolute()} --gpu no', capture_output=True, text=True, shell=True)
        logging.info(result.stdout)
        logging.info(result.stderr)

    labels = np.load(ex_output_data_path)

    assert np.array_equal(labels, ex_prediction_data)

    shutil.rmtree(model_folder_path)
    os.remove(ex_output_data_path)


@pytest.mark.usefixtures('ex_prediction_data_gpu', 'ex_input_data_path')
def test_prediction_default_tiff_gpu(ex_prediction_data_gpu, ex_input_data_path):
    with ex_input_data_path as path:
        ex_output_data_path = './ex_output_data.tiff'
        subprocess.run(f'stardist-cli -i {path.absolute()} -o {ex_output_data_path}', shell=True)

    labels = np.array(tiff.imread(ex_output_data_path))

    assert np.array_equal(labels, ex_prediction_data_gpu)

    os.remove(ex_output_data_path)
