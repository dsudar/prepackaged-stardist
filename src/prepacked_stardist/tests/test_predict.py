import pytest
import numpy as np
from stardist.data import test_image_nuclei_2d as get_test_image
from prepacked_stardist import Stardist
from .fixtures.data import ex_prediction_data

@pytest.mark.usefixtures("ex_prediction_data")
def test_prediction_versatile_fluo(ex_prediction_data):
    algorithm = Stardist('2D_versatile_fluo', use_gpu='no');

    img = get_test_image()
    labels = algorithm.predict(img)

    assert np.array_equal(labels, ex_prediction_data)